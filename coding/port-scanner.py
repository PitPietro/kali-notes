import socket
import termcolor


def scan_single_target(target, ports):
    print('\n Starting Scan For ' + (str(target)))
    for port in range(1, ports):
        scan_port(target, port)


def scan_port(ip_address, port):
    try:
        my_socket = socket.socket()
        my_socket.connect((ip_address, port))
        print("[+] {} port opened".format( str(port) ))
        my_socket.close()
    except:
        pass


user_targets = input("[*] Enter targets to scan (splitted by ,): ")
user_ports = int(input("[*] Enter how many ports you want to scan: "))

if ',' in user_targets:
    print(termcolor.colored(("[*] Scanning multiple targets"), 'green'))
    for target_ip in user_targets.split(','):
        scan_single_target(target_ip.strip(' '), user_ports)
else:
    scan_single_target(user_targets, user_ports)
