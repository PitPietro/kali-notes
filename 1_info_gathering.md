# 1. Information Gathering
Gather information about the target is the 1st step of **Penetration testing**.
You can read about many different tools in the official [Kali Tools Doc](https://tools.kali.org/information-gathering) page.

## 1.1. Active Information Gathering
Directly get information from the target:
Kali <-------------------------> Target

## 1.2. Passive Information Gathering
The information go throught a middle source (a search engine, a person, ...)
Kali <----> Middle Source <----> Target

## 1.3. Goals
What information you need to find?
1. IP Address/ IP Addresses
2. The **technologies** of the target:
    - The target is a company: Operative Systems mounted on the machines, softwares, ...
    - The target is a website: the programing language with which it's written, ...
3. Email and phone numbers (of the employees)

## 1.4. Perform Information Gathering
First of all, you need to select a target. Since you want to obtain its IP address, it will be a server or a website.
Let's select a target: `google.com`.

### 1.4.1. Obtain IP Address
#### 1.4.1.1. Active
##### 1.4.1.1.1. Ping
```bash
ping google.com
```

It will give as result:
```bash
PING google.com (216.58.206.78) 56(84) bytes of data.
64 bytes from lhr35s11-in-f14.1e100.net (216.58.206.78): icmp_seq=1 ttl=115 time=17.2 ms
64 bytes from mil07s08-in-f14.1e100.net (216.58.206.78): icmp_seq=2 ttl=115 time=15.5 ms
64 bytes from mil07s08-in-f14.1e100.net (216.58.206.78): icmp_seq=3 ttl=115 time=15.5 ms
64 bytes from mil07s08-in-f14.1e100.net (216.58.206.78): icmp_seq=4 ttl=115 time=15.3 ms
^C
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 15.298/15.852/17.174/0.766 ms

```

**Please Note**: Even a website does not ping back, it could be online anyway. Some website block *pinging*.

##### 1.4.1.1.2. NSLookup
```bash
sudo apt install dnsutils
```

Then you can type:
```bash
nslookup google.com
```

It will give as result:
```bash
Server:         192.168.1.254
Address:        192.168.1.254#53

Non-authoritative answer:
Name:   google.com
Address: 216.58.209.46
Name:   google.com
Address: 2a00:1450:4002:809::200e
```

The first couple of lines are info about you, while the other info below refers to the target.

#### 1.4.1.2. Passive
##### 1.4.1.2.1. IP Info website
Open a browser at [ipinfo.info](https://ipinfo.info/html/ip_checker.php) and insert the URL in the box near the `Check` button.
It will give a lot of information about the target, not just the IP Address.

##### 1.4.1.2.2. Whois
```bash
whois google.com
```

### 1.4.2. WhatWeb Tool
You can read about this tool on the official [Kali Tools Doc](https://tools.kali.org/web-applications/whatweb) page.
To use this tool, open a Terminal window and type:
```bash
whatweb --help
```

The defaut *aggression level* is **Stealthy** which is the best choose to perform an information gathering. It makes a single HTTP request per target and it follows redirects.

#### 1.4.2.1. Example of usage
```bash
whatweb google.com
```

It will give as result:
```bash
http://google.com [301 Moved Permanently] Country[UNITED STATES][US], HTTPServer[gws], IP[216.58.208.142], RedirectLocation[http://www.google.com/], Title[301 Moved], X-Frame-Options[SAMEORIGIN], X-XSS-Protection[0]                                                                                                                               
http://www.google.com/ [200 OK] Cookies[NID], Country[UNITED STATES][US], HTML5, HTTPServer[gws], HttpOnly[NID], IP[216.58.198.36], Script, Title[Google], X-Frame-Options[SAMEORIGIN], X-XSS-Protection[0]    
```

If you choose a different target, for example `facebook.com`, you'll get a more detailed output.
```bash
whatweb facebook.com
```

It will give as result:
```bash
http://facebook.com [301 Moved Permanently] Country[IRELAND][IE], IP[31.13.86.36], RedirectLocation[https://facebook.com/], UncommonHeaders[x-fb-debug,alt-svc]
https://facebook.com/ [301 Moved Permanently] Country[IRELAND][IE], IP[31.13.86.36], RedirectLocation[https://www.facebook.com/], Strict-Transport-Security[max-age=15552000; preload], UncommonHeaders[x-fb-debug,alt-svc]                                                                                                                           
https://www.facebook.com/ [200 OK] Country[IRELAND][IE], HTML5, IP[31.13.86.36], Meta-Refresh-Redirect[/?_fb_noscript=1], OpenSearch[/osd.xml], PasswordField[pass], Script, Strict-Transport-Security[max-age=15552000; preload], UncommonHeaders[x-content-type-options,x-fb-debug,alt-svc], X-Frame-Options[DENY], X-XSS-Protection[0]
https://www.facebook.com/?_fb_noscript=1 [200 OK] Cookies[noscript], Country[IRELAND][IE], HTML5, IP[31.13.86.36], OpenSearch[/osd.xml], PasswordField[pass], Script, Strict-Transport-Security[max-age=15552000; preload], UncommonHeaders[x-content-type-options,x-fb-debug,alt-svc], X-Frame-Options[DENY], X-XSS-Protection[0]
```

Moreover, if you use the **verbose** mode, you'll get a more readable way.
```bash
whatweb facebook.com --verbose
whatweb facebook.com -v
```

Since the result is 200+ lines, you could think about redirecting the output into a txt file using the `>` (or `>>`) operator. But it's not a good idea since it also saves some strange characters because of the tabs and the square brackets that `whatweb` command outputs.
See the [logging](#whatweb-logging) slice for a better solution.

#### 1.4.2.2. Test a range of IPs
You can scan the IP addresses of your local network by giving to the command a range of IPs. Let's type `sudo ifconfig` to find out the IPs range.

```bash
┌──(elliot㉿kalihack)-[~]
└─$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.20  netmask 255.255.255.0  broadcast 192.168.0.255
# ...
```

As you can see from the subnet mask (which is `255.255.255.0`), `192.168.0.` won't never change. I got ony the last number to scan for and it is in a range from `1` to `255`. Let's scan the network:
```bash
whatweb 192.168.0.1-192.168.0.255
```

It will give as result:
```bash
ERROR Opening: http://192.168.0.5 - Connection refused - connect(2) for "192.168.0.5" port 80
ERROR Opening: http://192.168.0.4 - Connection refused - connect(2) for "192.168.0.4" port 80
ERROR Opening: http://192.168.0.20 - Connection refused - connect(2) for "192.168.0.20" port 80
http://192.168.0.1 [200 OK] Content-Language[en], Country[RESERVED][ZZ], HTTPServer[HTTP Server], IP[192.168.0.1], Meta-Refresh-Redirect[/ui/dboard], X-Frame-Options[DENY]
ERROR Opening: http://192.168.0.14 - No route to host - connect(2) for "192.168.0.14" port 80
ERROR Opening: http://192.168.0.7 - No route to host - connect(2) for "192.168.0.7" port 80
# ...
```

Of course, you'd better have to press `Ctrl` + `C` to cancel the execution of the command.
All the **ERROR**s you get, are all the hosts that it tries to scan, but could not manage to since they do not exists.
To prevent the command to print out all the non-existent hosts, just add the `--no-errors` flag.
```bash
whatweb 192.168.0.1-192.168.0.255 --no-errors
```

You could also want to increase the *aggression level* by adding the `--aggression [number]` flag.
```bash
whatweb 192.168.0.1-192.168.0.255 --aggression 3
```

Of course you can combine all the flags which you have seen so far:
```bash
whatweb 192.168.0.1-192.168.0.255 -v --aggression 3 --no-errors
```

#### 1.4.2.3. Whatweb Logging
`whatweb` has a lot of logging options, let's try them all:
```bash
whatweb 192.168.0.1-192.168.0.255 --log-verbose=result.log
```

You could also avoid to specify the `.log` extension. But if you want to version your research online, you should not load the logs as well. For this reason, you could write a tiny `.gitignore`.

### 1.4.3. Gather Email
#### 1.4.3.1. theHarvester
You can read about this tool on the official [Kali Tools Doc](https://tools.kali.org/information-gathering/theharvester) page.
To use this tool, open a Terminal window and type:
```bash
theHarvester --help
```

As you can read from the help menu, you must specify the `-d` (or `--domain`) flag which is the target domain.
Another important flag is `-b` (or `--source`) which specify where you want to search for emails.
If `theHarvester` does not give you any usefull result, try to run it again (maybe some hours later or even a day later).

##### 1.4.3.1.1. Example of usage
```bash
theHarvester -d facebook.com -b all
```

```bash
theHarvester -d facebook.com -b google
```

#### 1.4.3.2. hunter.io
Open a browser page at [hunter.io](https://hunter.io/search) and create an account (otherwise it will show only five results).
Be sure to use a professional mail, otherwise you be added to a sort of blacklist as you can read from the sign up form (I used my company once).
You can even download the Firefox [addons](https://addons.mozilla.org/en-US/firefox/addon/hunterio/)!

#### 1.4.3.3. Email Scraper from Ethical Hacking Bootcamp
This email scraper comes from the **Complete Ethical Hacking Bootcamp 2021: Zero to Mastery** source course.

Let's copy/paste the stricpt in `Documents/tools` folder.
Its content is:
```python
from bs4 import BeautifulSoup
import requests
import requests.exceptions
import urllib.parse
from collections import deque
import re

user_url = str(input('[+] Enter Target URL To Scan: '))
urls = deque([user_url])

scraped_urls = set()
emails = set()

count = 0
try:
    while len(urls):
        count += 1
        if count == 100:
            break
        url = urls.popleft()
        scraped_urls.add(url)

        parts = urllib.parse.urlsplit(url)
        base_url = '{0.scheme}://{0.netloc}'.format(parts)

        path = url[:url.rfind('/')+1] if '/' in parts.path else url

        print('[%d] Processing %s' % (count, url))
        try:
            response = requests.get(url)
        except (requests.exceptions.MissingSchema, requests.exceptions.ConnectionError):
            continue

        new_emails = set(re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", response.text, re.I))
        emails.update(new_emails)

        soup = BeautifulSoup(response.text, features="lxml")

        for anchor in soup.find_all("a"):
            link = anchor.attrs['href'] if 'href' in anchor.attrs else ''
            if link.startswith('/'):
                link = base_url + link
            elif not link.startswith('http'):
                link = path + link
            if not link in urls and not link in scraped_urls:
                urls.append(link)
except KeyboardInterrupt:
    print('[-] Closing!')

for mail in emails:
    print(mail)

```

It has some room of improvement, but for now let's run it as it is. Gather info from `https://github.com/` for example:
```bash
python3 email-scraper.py
```

It will give as result:
```bash
[+] Enter Target URL To Scan:
```

And then, after pasting the URL:
```bash
[+] Enter Target URL To Scan: https://github.com/
[1] Processing https://github.com/
[2] Processing https://github.com/#start-of-content
[3] Processing https://docs.github.com/articles/supported-browsers
[4] Processing https://github.com/join?ref_cta=Sign+up&ref_loc=header+logged+out&ref_page=%2F&source=header-home
[5] Processing https://github.com/features
[6] Processing https://github.com/features/code-review/
# ...
mathews.kyle+gatsbybot@gmail.com
46536646+springstan@users.noreply.github.com
marc.cornella@live.com
announce@tensorflow.org
privacy@github.com
blainekasten@gmail.com
# ...
```

### 1.4.4. Download Other Tools
The best website you can seach for information gathering tools is [GitHub](https://github.com).
The searches can be:
1. [github.com/topics/information-gathering](https://github.com/topics/information-gathering)
2. [github.com/topics/information-gathering-tools](https://github.com/topics/information-gathering-tools)

### 1.4.4.1. RED_HAWK
Let's try out [RED_HAWK](https://github.com/Tuhinshubhra/RED_HAWK).
You always have to carefully read the `RAEDME.md` file: it contains all the information you need to download (clone), install and use a GitHub project.

```bash
# clone the repository inside a specific Document's subfolder
cd Documents
mkdir tools
cd tools
git clone https://github.com/Tuhinshubhra/RED_HAWK.git
cd RED_HAWK

# run the main script
php rhawk.php

# the command above will probably tell you some packages are missing so type
fix

# restart the tool
clear && php rhawk.php
```

It's very cool!
```bash

           All In One Tool For Information Gathering And Vulnerability Scanning
                                                              .  .  .  .
                                                              .  |  |  .
                                                           .  |        |  .
                                                           .              .
                                              @@@@@      . |  (\.|\/|./)  | .   ___   ____
  ██████╗ ███████╗██████╗    ###     ###    @@@@ @@@@    .   (\ |||||| /)   .  |   | /   /
  ██╔══██╗██╔════╝██╔══██╗   ###     ###   @@@@   @@@@   |  (\  |/  \|  /)  |  |   |/   /
  ██████╔╝█████╗  ██║  ██║   ###########   @@@@@@@@@@@     (\             )    |       /
  ██╔══██╗██╔══╝  ██║  ██║   ###########   @@@@@@@@@@@    (\  Ver  2.0.0  /)   |       \
  ██║  ██║███████╗██████╔╝   ###     ###   @@@     @@@     \      \/      /    |   |\   \
  ╚═╝  ╚═╝╚══════╝╚═════╝    ###     ###   @@@     @@@      \____/\/\____/     |___| \___\
                                                                |0\/0|
         {C} Coded By - R3D#@X0R_2H1N A.K.A Tuhinshubhra         \/\/
                                                                  \/  [$] Shout Out - You ;)

  

[#] Enter The Website You Want To Scan : 

```

Let's follow the instruction and explore the tool!

### 1.4.4.2. Sherlock
Let's try out [sherlock](https://github.com/sherlock-project/sherlock).

```bash
# clone the repository inside a specific Document's subfolder
cd Documents/tools
git clone https://github.com/sherlock-project/sherlock.git
cd sherlock

# install the requirements
$ python3 -m pip install -r requirements.txt

# show the help message
python3 sherlock --help
```

You can search for Bill Gates accounts since I know his Instagram username is `thisisbillgates`:
```bash
python3 sherlock thisisbillgates
```

Or you can seach for a casual name, for example `marcorossi`:
```bash
python3 sherlock marcorossi
```

It will give as result a lot of websites:
```bash
[*] Checking username marcorossi on:
[+] 500px: https://500px.com/p/marcorossi
[+] 9GAG: https://www.9gag.com/u/marcorossi
[+] About.me: https://about.me/marcorossi
[+] Academia.edu: https://independent.academia.edu/marcorossi
[+] Apple Discussions: https://discussions.apple.com/profile/marcorossi
[+] Archive.org: https://archive.org/details/@marcorossi
[+] AskFM: https://ask.fm/marcorossi
[+] Audiojungle: https://audiojungle.net/user/marcorossi
[+] Behance: https://www.behance.net/marcorossi
[+] BitBucket: https://bitbucket.org/marcorossi/
[+] Blogger: https://marcorossi.blogspot.com
[+] BuzzFeed: https://buzzfeed.com/marcorossi
[+] CNET: https://www.cnet.com/profiles/marcorossi/
[+] Carbonmade: https://marcorossi.carbonmade.com
[+] Chess: https://www.chess.com/member/marcorossi
[+] Codecademy: https://www.codecademy.com/profiles/marcorossi
[+] DailyMotion: https://www.dailymotion.com/marcorossi
[+] DeviantART: https://marcorossi.deviantart.com
[+] Discogs: https://www.discogs.com/user/marcorossi
# ...
```

You can explore what flags do to select only certain social networks, output the result of the search in a file and so on.

### 1.4.5. Open Source Intelligence Tools
Even though it does not refer only to information gathering, I would like to close this section with a list of usefull OSI tools (that won't be treated here).
On the [SecurityTrails](https://securitytrails.com/blog) blog you can find the [Top 25 OSINT Tools for Penetration Testing](https://securitytrails.com/blog/osint-tools).
