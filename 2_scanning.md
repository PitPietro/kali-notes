# 2. Scanning
Scan the target is the 2nd step of **Penetration testing**. Is a deeper information gathering which is focused on gather information about the *techology* side of the target.

## 2.1. Theory of Scanning
### 2.1.1. TCP & UDP
**TCP** stands for `Transmission Control Protocol`. It's the most common way of comunication over the Internet.
It is based on **three way handshake** which consists in (off course) three steps:
1. **SYN**: the client wants to establish a connection with a server so he sends a segment with **SYN**, which notify the server that client wants to start communication. **SYN** also tells the server what is the sequence number of the client.
2. **SYN/ACK**: the server respond to the client request (similar to **SYN** step).
3. **ACK**: the client acknowledges the response of server and establish a reiable connection with it.

**Please Note**: **SYN** stands for `Synchronize Sequence Number`.

**UDP** stands for `User Datagram Protocol`. It's faster than TCP but could lost some packages. It's used for video streaming and so on.

### 2.1.2. Firewall & IDS
Firewall is a network security system which monitors network traffic. It's based on security rooles.
A firewall can be:
1. network firewall (montors the traffic from different networks)
2. host-based firewall (monitors the trafic in and out a specific host)

IDS stands for Intrusion Detection System.

## 2.2. Goals of Scanning
Since **scanning** is not allowed on any target you want (unless you have permissions), you'll need to download a virtual machine and scan it.
You'll send TCP and UDP packages through the open ports to the target to find out usefull information.

The goal of scanning is find virtual ports which are open and find out what software (and what version of it) are running on that open port.

Example of ports are:
- port 21: File Transfer Protocol (FTP)
- port 22: Secure SHell (SSH)
- port 25: Simple Mail Transfer Protocol (SMTP)
- port 53: Domain Name Server (DNS)
- port 80: webserver through HTTP
- port 443: webserver through HTTPS

Every machine has 65535 ports, which comunicates through TCP or UDP.

## 2.3. Install a vurnerable virtual machine
In order to follow the tutorial, you need a vurnerable virtual machine. You can find many of them by searching online. Udemy course suggest this one: [Metasploitable](https://information.rapid7.com/download-metasploitable-2017.html?LS=1631875&CS=web), but you have to fill the form with sensitive information.
If you do not want to fill that form, you can download it directly from [sourceforge.net](https://sourceforge.net/projects/metasploitable/).
Metasploitable is an already configured Virtual Machine, so add it to Oracle Virtual Box and run it. Be sure to change the network settings and switch from `NAT` to `Bridge Adapter` so that both the virtual machines (Kali and this one) are on the same local network and can access eachother.
Metasploitable is a CLI version of Linux (no desktop). Remind that login and password are both "msfadmin".
If you run `ifconfig` on both Kali and Metasploitable, you can notice that both IP addresses start with the same triad of numbers (in my case `192.168.1.`).

## 2.4. Perform Scanning
Now that you setup the virtual machine, you can proceed to actually make some scanning! The fist step is to find out what devices are connected to your local network.

### 2.4.1. Scanning a LAN
Before scanning a machine connected on your same network, you need to know how many hosts you have on your network and what are their IP addresses.
The craziest way to do it, could be to ping every possible IP address!

#### 2.4.1.1. Arp Tool
```bash
arp --help
```

If it doesn't work, try to run it with `sudo` privileges.

Run the following command to find out the devices that are on your same LAN. It will only print the IP addresses of the devices you have already communicated with.
```bash
arp -a
```

#### 2.4.1.2. Netdiscover
Since `arp` could be quite useless, you can use another usefull tool: Netdiscover!
```bash
sudo netdiscover
```

This time, you **must** run it with `sudo` privileges.
It will automatically scan you network and print the IP address, the MAC address and the MAC vendor or hostname.

It will give as result:
```bash
Currently scanning: 172.18.216.0/16   |   Screen View: Unique Hosts                                                                                                      
                                                                                                                                                                          
 1438 Captured ARP Req/Rep packets, from 12 hosts.   Total size: 86280                                                                                                    
 _____________________________________________________________________________
   IP            At MAC Address     Count     Len  MAC Vendor / Hostname      
 -----------------------------------------------------------------------------
 192.168.1.65    xx:xx:xx:xx:xx:xx    265   15900  COMPAL INFORMATION (KUNSHAN) CO., LTD.                                                                                 
 192.168.1.70    08:00:27:0f:3c:b5      9     540  PCS Systemtechnik GmbH                                                                                                 
 192.168.1.204   yy:yy:yy:yy:yy:yy    145    8700  TP-LINK TECHNOLOGIES CO.,LTD.                                                                                          
 192.168.1.101   zz:zz:zz:zz:zz:zz     16     960  Amazon Technologies Inc.                                                                                               
 192.168.1.254   aa:aa:aa:aa:aa:aa    782   46920  Technicolor                                                                                                            
 192.168.1.167   bb:bb:bb:bb:bb:bb     13     780  Chicony Electronics Co., Ltd.                                                                                          
 192.168.1.234   cc:cc:cc:cc:cc:cc    132    7920  Intel Corporate                                                                                                        
 192.168.1.66    dd:dd:dd:dd:dd:dd     20    1200  Samsung Electronics Co.,Ltd                                                                                            
 192.168.1.78    ee:ee:ee:ee:ee:ee     32    1920  HUAWEI TECHNOLOGIES CO.,LTD                                                                                            
 192.168.1.96    ff:ff:ff:ff:ff:ff      6     360  Intel Corporate                                                                                                        
 0.0.0.0         gg:gg:gg:gg:gg:gg      5     300  Intel Corporate                                                                                                        
 0.0.0.0         hh:hh:hh:hh:hh:hh     13     780  HUAWEI TECHNOLOGIES CO.,LTD  
```

Obviously, I have hidden all MAC addresses except Metasploitable's which has **PCS Systemtechnik GmbH** as MAC Vendor.
**Please Note**: I found a sharp deterioration of the network connection while running `netdiscover`.

### 2.4.2. Nmap
Nmap is a network mapper, network scanner and it's used to discover hosts and services on a network by sending packets and analyzing responses. Moreover, it's free and open source!

```bash
nmap --help
```

As you can see, Nmap is not only used to scan a network, but it has lots of different flags for different usage! I could also make a focus on this tool later on (in a dedicated file).

#### 2.4.2.1. Nmap Manual
```bash
man nmap
```

It explains every `nmap` option in great detail.
Under the **PORT SCANNING BASICS** menu, are listed the **six port states recognized by Nmap** which are:
1. open
2. closed
3. filtered
4. unfiltered
5. open|filtered
6. closed|filtered

**PORT SCANNING TECHNIQUES** menu will show all the scanning techniques you can perform with `nmap`. Depending on the target and what you want to get from it, you will have to choose a different type of scanning.

#### 2.4.2.2. Scan a single IP address with Nmap
Let's focus on **TARGET SPECIFICATION** help's submenu for the flags you need to use right now.
Thanks to `netdiscover`, you already found the Metasploitable's IP address, which in my case is `192.168.1.70` as you can read above. Since you're running Metasploitable on Virtual Machine, you can check its IP address running `ifconfig` on it. Both IP addresses must match.

Let's scan the target!
```bash
nmap [IP address]
```

In my case, I have to run 
```bash
nmap 192.168.1.70
```

It will give as result:
```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-13 17:24 CET
Nmap scan report for 192.168.1.70
Host is up (0.00032s latency).
Not shown: 977 closed ports
PORT     STATE SERVICE
21/tcp   open  ftp
22/tcp   open  ssh
23/tcp   open  telnet
25/tcp   open  smtp
53/tcp   open  domain
80/tcp   open  http
111/tcp  open  rpcbind
139/tcp  open  netbios-ssn
445/tcp  open  microsoft-ds
512/tcp  open  exec
513/tcp  open  login
514/tcp  open  shell
1099/tcp open  rmiregistry
1524/tcp open  ingreslock
2049/tcp open  nfs
2121/tcp open  ccproxy-ftp
3306/tcp open  mysql
5432/tcp open  postgresql
5900/tcp open  vnc
6000/tcp open  X11
6667/tcp open  irc
8009/tcp open  ajp13
8180/tcp open  unknown

Nmap done: 1 IP address (1 host up) scanned in 0.13 seconds
```

Depending on where the target is and many other variables, running this command could take a lot of time (even hours!) but in this case it has taken less than a second.
However, `nmap` tells you the port, its state and what service is running on it. As you can see, the port 80 is hosting a http server, by means the target is probably running a website. To check it out, let's open a Firefox page at `[IP address]:80`.
In my case, I can type on the terminal:
```bash
firefox 192.168.1.70:80
```

And you can see a tiny website.

**Please Note**: `nmap` will only scan the 1000 most common ports if you do not specify anything about: scanning all 65535 ports everytime would be quite useless and time-wasting.

#### 2.4.2.3. Scan a range of IP addresses with Nmap
```bash
nmap [starting IP address]/[subnet mask]

# scan all IPs from 192.168.1.1 to 192.168.1.225
nmap 192.168.1.1/24
```

This scan will take more time than the first one since it will scan all the devices connected to the network.
You will find something like before, for the devices which have open ports and result like this one below for devices which have no open ports.
```bash
# ...

Nmap scan report for HUAWEI_P9_lite.lan (192.168.1.233)
Host is up (0.013s latency).
All 1000 scanned ports on HUAWEI_P9_lite.lan (192.168.1.233) are closed

# ...
```

As you can see, my smartphone has no open port.

#### 2.4.2.4. TCP SYN Scann
```bash
sudo nmap -sS [IP_address]

# scan the vurnerable machine
sudo nmap -sS 192.168.1.70
```

As you can see from the `nmap --help` message, under **SCAN TECHNIQUES**, the `-sS` flag trigger the **TCP SYN scan**.
This type of scan, never realy opens a TCP connection with the target: it performs the 1st handshake by sending SYN. If the target send SYN/ACK back (the 2nd handshake) for a certain port, it means that the port is listening or open. If the target sends RST back (Reset), it means that the port is closed. If the target does not give any response, the port is marked as *filtered* (it could happen if the port is protected by a firewall).

**Please Note**: You must have `sudo` privileges to run this command since you must not respond to SYN/ACK handshake. You won't establish a connection with the target.

It will give as result the same as above, the only difference is the execution time:
```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-14 21:47 CET
Nmap scan report for 192.168.1.70
Host is up (0.00033s latency).
Not shown: 977 closed ports
PORT     STATE SERVICE
21/tcp   open  ftp
22/tcp   open  ssh
23/tcp   open  telnet
25/tcp   open  smtp
53/tcp   open  domain
80/tcp   open  http
111/tcp  open  rpcbind
139/tcp  open  netbios-ssn
445/tcp  open  microsoft-ds
512/tcp  open  exec
513/tcp  open  login
514/tcp  open  shell
1099/tcp open  rmiregistry
1524/tcp open  ingreslock
2049/tcp open  nfs
2121/tcp open  ccproxy-ftp
3306/tcp open  mysql
5432/tcp open  postgresql
5900/tcp open  vnc
6000/tcp open  X11
6667/tcp open  irc
8009/tcp open  ajp13
8180/tcp open  unknown
MAC Address: 08:00:27:0F:3C:B5 (Oracle VirtualBox virtual NIC)

Nmap done: 1 IP address (1 host up) scanned in 0.68 seconds
```

#### 2.4.2.5. TCP Connection Scann
```bash
nmap -sT [IP_address]

# scan the vurnerable machine
nmap -sT 192.168.1.70
```

As you can see from the `nmap --help` message, under **SCAN TECHNIQUES**, the `-sT` flag trigger the **Connect() scan**.
This type of scan, opens a TCP connection with the target so it's easly detected.
However, it works as the **TCP SYN scan**.

**Please Note**: You do not require `sudo` privileges to run this command since you must respond to SYN/ACK handshake. You will establish a connection with the target.

It will give as result the same as above, the only difference is the execution time: in my case `0.24` seconds in place of `0.68` as you can see above.

Both those scanns, unlike the simple `nmap [IP]`, tells you the MAC address of the scanned IP.

#### 2.4.2.6. UDP Scann
```bash
nmap -sU [IP_address]

# scan the vurnerable machine
nmap -sU 192.168.1.70
```

Since UDP is more slower and difficult than TCP, this type of scan is very unpopular. Sometimes UDP is ignored while developing security for the ports, for this reason there are a lot of exploitable UDP ports.
Note that his type of scan will require `sudo` privileges.

This scan will take time, so make yourself comfortable. If you press the **UP arrow** key while executing, it will tell you what percentage it is at.

It will give as result:
```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-16 22:46 CET

# when you press the up arrow
Stats: 0:05:10 elapsed; 0 hosts completed (1 up), 1 undergoing UDP Scan
UDP Scan Timing: About 30.86% done; ETC: 23:03 (0:11:35 remaining)

# when scanning ends
Nmap scan report for 192.168.1.70
Host is up (0.00042s latency).
Not shown: 993 closed ports
PORT     STATE         SERVICE
53/udp   open          domain
68/udp   open|filtered dhcpc
69/udp   open|filtered tftp
111/udp  open|filtered rpcbind
137/udp  open|filtered netbios-ns
138/udp  open|filtered netbios-dgm
2049/udp open          nfs
MAC Address: 08:00:27:0F:3C:B5 (Oracle VirtualBox virtual NIC)

Nmap done: 1 IP address (1 host up) scanned in 1075.80 seconds
```

#### 2.4.2.7 Discover target OS
```bash
sudo nmap -O [IP_address]

# scan the vurnerable machine
sudo nmap -O 192.168.1.70
```

`nmap` have a database with thousands on known operating systems it compares with the target. To allow this type of scan, the target must have at least one port open and one port closed.

It will give as result:
```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-16 23:39 CET
Nmap scan report for 192.168.1.70
Host is up (0.00039s latency).
Not shown: 977 closed ports
PORT     STATE SERVICE
21/tcp   open  ftp
# ...
8180/tcp open  unknown
MAC Address: 08:00:27:0F:3C:B5 (Oracle VirtualBox virtual NIC)
Device type: general purpose
Running: Linux 2.6.X
OS CPE: cpe:/o:linux:linux_kernel:2.6
OS details: Linux 2.6.9 - 2.6.33
Network Distance: 1 hop

OS detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 1.82 seconds
```

`nmap` also figures out the target is a virtual machine: Oracle VirtualBox's MAC addresses always starts with `08:00:27`. It helps you realize the target is a virtual machine and not a physical machine, by means the target could be an *honeypot*: a purposely vurnerable virtual environment used to luring hackers in order to find out whether the target's owner has being attacked.

Let's try to scan your the IP address of the physical machine which runs Virtual Box (in my case is a Windows 10 machine).
```bash
# physical machine IP address (Win 10)
sudo nmap -O 192.168.1.65
```

It will give as result:
```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-16 23:53 CET
Nmap scan report for DESKTOP-KTP8IVF.lan (192.168.1.65)
Host is up (0.00021s latency).
Not shown: 999 filtered ports
PORT     STATE SERVICE
6646/tcp open  unknown
MAC Address: 98:28:A6:3E:9A:1E (Compal Information (kunshan))
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Device type: WAP|phone
Running: Linux 2.4.X|2.6.X, Sony Ericsson embedded
OS CPE: cpe:/o:linux:linux_kernel:2.4.20 cpe:/o:linux:linux_kernel:2.6.22 cpe:/h:sonyericsson:u8i_vivaz
OS details: Tomato 1.28 (Linux 2.4.20), Tomato firmware (Linux 2.6.22), Sony Ericsson U8i Vivaz mobile phone
Network Distance: 1 hop

OS detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 17.68 seconds
```

This time `nmap` didn't manage to find out the target's OS since it has no open port: all ports are close or filtered.

#### 2.4.2.8. Discover version of service with Nmap
```bash
sudo nmap -sV [IP_address]

# scan the vurnerable machine
sudo nmap -sV 192.168.1.70
```

`nmap` can discover the version of a service running on an open port of the target. By knowing the version of a service (for example apache or django webserver), you can search for knowing vurnerabilities of that specific version.
This type of scan could take a lot of time since it's deeply scanning the target. Unlike other `nmap` types of scan, this one added the **VERSION** column.

It will give as result:
```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-17 21:40 CET
Nmap scan report for 192.168.1.70
Host is up (0.00027s latency).
Not shown: 977 closed ports
PORT     STATE SERVICE     VERSION
21/tcp   open  ftp         vsftpd 2.3.4
22/tcp   open  ssh         OpenSSH 4.7p1 Debian 8ubuntu1 (protocol 2.0)
23/tcp   open  telnet      Linux telnetd
25/tcp   open  smtp        Postfix smtpd
53/tcp   open  domain      ISC BIND 9.4.2
80/tcp   open  http        Apache httpd 2.2.8 ((Ubuntu) DAV/2)
111/tcp  open  rpcbind     2 (RPC #100000)
139/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
512/tcp  open  exec?
513/tcp  open  login       OpenBSD or Solaris rlogind
514/tcp  open  tcpwrapped
1099/tcp open  java-rmi    GNU Classpath grmiregistry
1524/tcp open  bindshell   Metasploitable root shell
2049/tcp open  nfs         2-4 (RPC #100003)
2121/tcp open  ftp         ProFTPD 1.3.1
3306/tcp open  mysql       MySQL 5.0.51a-3ubuntu5
5432/tcp open  postgresql  PostgreSQL DB 8.3.0 - 8.3.7
5900/tcp open  vnc         VNC (protocol 3.3)
6000/tcp open  X11         (access denied)
6667/tcp open  irc         UnrealIRCd
8009/tcp open  ajp13       Apache Jserv (Protocol v1.3)
8180/tcp open  http        Apache Tomcat/Coyote JSP engine 1.1
MAC Address: 08:00:27:0F:3C:B5 (Oracle VirtualBox virtual NIC)
Service Info: Hosts:  metasploitable.localdomain, irc.Metasploitable.LAN; OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 62.89 seconds
```

You can also increase the intensity of *scanning versions* in a range from 0 to 9 with the `--version-intensity` flag (default is 7). Scanning Metasploitable will give the same result as before and will increase the execution time, so be sure to use it only when realy needed.
```bash
sudo nmap -sV --version-intensity [n] [IP_address]

# scan the vurnerable machine
sudo nmap -sV --version-intensity 9 192.168.1.70
```

You can also use the so-called **aggressive** option with the `-A` flag. It enables the OS detection (`-O` flag), the version detection (`-sV` flag) and the *nmap script scanning*. Since this scan is very aggressive and easly detecteble, do not use on target you don't have permission to scan.
```bash
sudo nmap -A [IP_address]

# scan the vurnerable machine
sudo nmap -A 192.168.1.70
```

It will give as result:
```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-17 22:03 CET
Nmap scan report for 192.168.1.70
Host is up (0.00039s latency).
Not shown: 977 closed ports
PORT     STATE SERVICE     VERSION
21/tcp   open  ftp         vsftpd 2.3.4
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to 192.168.1.228
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      vsFTPd 2.3.4 - secure, fast, stable
|_End of status
22/tcp   open  ssh         OpenSSH 4.7p1 Debian 8ubuntu1 (protocol 2.0)
| ssh-hostkey: 
|   1024 60:0f:cf:e1:c0:5f:6a:74:d6:90:24:fa:c4:d5:6c:cd (DSA)
|_  2048 56:56:24:0f:21:1d:de:a7:2b:ae:61:b1:24:3d:e8:f3 (RSA)
23/tcp   open  telnet      Linux telnetd
25/tcp   open  smtp        Postfix smtpd
|_smtp-commands: metasploitable.localdomain, PIPELINING, SIZE 10240000, VRFY, ETRN, STARTTLS, ENHANCEDSTATUSCODES, 8BITMIME, DSN, 
|_ssl-date: 2020-12-17T21:04:21+00:00; +1s from scanner time.
| sslv2: 
|   SSLv2 supported
|   ciphers: 
|     SSL2_RC2_128_CBC_WITH_MD5
|     SSL2_RC4_128_EXPORT40_WITH_MD5
|     SSL2_DES_64_CBC_WITH_MD5
|     SSL2_RC2_128_CBC_EXPORT40_WITH_MD5
|     SSL2_DES_192_EDE3_CBC_WITH_MD5
|_    SSL2_RC4_128_WITH_MD5
53/tcp   open  domain      ISC BIND 9.4.2
| dns-nsid: 
|_  bind.version: 9.4.2
80/tcp   open  http        Apache httpd 2.2.8 ((Ubuntu) DAV/2)
|_http-server-header: Apache/2.2.8 (Ubuntu) DAV/2
|_http-title: Metasploitable2 - Linux
111/tcp  open  rpcbind     2 (RPC #100000)
| rpcinfo: 
|   program version    port/proto  service
|   100000  2            111/tcp   rpcbind
|   100000  2            111/udp   rpcbind
|   100003  2,3,4       2049/tcp   nfs
|   100003  2,3,4       2049/udp   nfs
|   100005  1,2,3      39735/tcp   mountd
|   100005  1,2,3      52532/udp   mountd
|   100021  1,3,4      36700/udp   nlockmgr
|   100021  1,3,4      39681/tcp   nlockmgr
|   100024  1          41899/udp   status
|_  100024  1          48686/tcp   status
139/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp  open  netbios-ssn Samba smbd 3.0.20-Debian (workgroup: WORKGROUP)
512/tcp  open  exec?
513/tcp  open  login?
514/tcp  open  tcpwrapped
1099/tcp open  java-rmi    GNU Classpath grmiregistry
1524/tcp open  bindshell   Metasploitable root shell
2049/tcp open  nfs         2-4 (RPC #100003)
2121/tcp open  ftp         ProFTPD 1.3.1
3306/tcp open  mysql       MySQL 5.0.51a-3ubuntu5
| mysql-info: 
|   Protocol: 10
|   Version: 5.0.51a-3ubuntu5
|   Thread ID: 10
|   Capabilities flags: 43564
|   Some Capabilities: Speaks41ProtocolNew, SupportsTransactions, SupportsCompression, Support41Auth, LongColumnFlag, SwitchToSSLAfterHandshake, ConnectWithDatabase
|   Status: Autocommit
|_  Salt: Sal|J;)[n8h''/T9 Xyx
5432/tcp open  postgresql  PostgreSQL DB 8.3.0 - 8.3.7
|_ssl-date: 2020-12-17T21:04:21+00:00; +1s from scanner time.
5900/tcp open  vnc         VNC (protocol 3.3)
| vnc-info: 
|   Protocol version: 3.3
|   Security types: 
|_    VNC Authentication (2)
6000/tcp open  X11         (access denied)
6667/tcp open  irc         UnrealIRCd
| irc-info: 
|   users: 1
|   servers: 1
|   lusers: 1
|   lservers: 0
|   server: irc.Metasploitable.LAN
|   version: Unreal3.2.8.1. irc.Metasploitable.LAN 
|   uptime: 0 days, 0:34:32
|   source ident: nmap
|   source host: Test-B2A13D6F.lan
|_  error: Closing Link: spejrjktj[kalihack.lan] (Quit: spejrjktj)
8009/tcp open  ajp13       Apache Jserv (Protocol v1.3)
|_ajp-methods: Failed to get a valid response for the OPTION request
8180/tcp open  http        Apache Tomcat/Coyote JSP engine 1.1
|_http-favicon: Apache Tomcat
|_http-server-header: Apache-Coyote/1.1
|_http-title: Apache Tomcat/5.5
MAC Address: 08:00:27:0F:3C:B5 (Oracle VirtualBox virtual NIC)
Device type: general purpose
Running: Linux 2.6.X
OS CPE: cpe:/o:linux:linux_kernel:2.6
OS details: Linux 2.6.9 - 2.6.33
Network Distance: 1 hop
Service Info: Hosts:  metasploitable.localdomain, irc.Metasploitable.LAN; OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_clock-skew: mean: 1h15m00s, deviation: 2h30m00s, median: 0s
|_nbstat: NetBIOS name: METASPLOITABLE, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| smb-os-discovery: 
|   OS: Unix (Samba 3.0.20-Debian)
|   Computer name: metasploitable
|   NetBIOS computer name: 
|   Domain name: localdomain
|   FQDN: metasploitable.localdomain
|_  System time: 2020-12-17T16:04:13-05:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_smb2-time: Protocol negotiation failed (SMB2)

TRACEROUTE
HOP RTT     ADDRESS
1   0.39 ms 192.168.1.70

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 73.39 seconds
```

#### 2.4.2.8. No port scann with Nmap
```bash
#both ways work the same
nmap -sn [starting IP address]/[subnet mask]
nmap -sn [first IP address]-[last IP address]

# scan all IPs from 192.168.1.1 to 192.168.1.225
nmap -sn 192.168.1.1/24
nmap -sn 192.168.1.1-255
```

It works as `netdiscover`: it discovers all the hosts that are up and running on the local network.

They'll give as result:
```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-18 09:33 CET
Nmap scan report for 192.168.1.1
Host is up (0.0014s latency).
Nmap scan report for 192.168.1.4
Host is up (0.0014s latency).
Nmap scan report for 192.168.1.5
Host is up (0.0013s latency).
Nmap scan report for 192.168.1.20
Host is up (0.00054s latency).
# ...
Nmap scan report for 192.168.1.252
Host is up (0.026s latency).
Nmap done: 256 IP addresses (14 hosts up) scanned in 5.54 seconds
```

#### 2.4.2.9. Filter Port Range with Nmap
```bash
nmap -p [port_number] [IP_address]

# scan the vurnerable machine
nmap -p 80 192.168.1.70
```

Specify the range of ports you want to scan. It this case you have specified only a port.
It wll give as result:
```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-18 11:20 CET
Nmap scan report for 192.168.1.70
Host is up (0.0024s latency).

PORT   STATE SERVICE
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.13 seconds
```

If you want to scan different ports, just separate them with a comma:
```bash
nmap -p [port_1],[port_2],[port_3] [IP_address]

# scan the vurnerable machine
nmap -p 22,80,100 192.168.1.70
```

It wll give as result:
```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-18 11:26 CET
Nmap scan report for 192.168.1.70
Host is up (0.0013s latency).

PORT    STATE  SERVICE
22/tcp  open   ssh
80/tcp  open   http
100/tcp closed newacct

Nmap done: 1 IP address (1 host up) scanned in 0.13 seconds
```

If you want to scan a range of ports, separate the first from the last with a dash:
```bash
nmap -p [first_port]-[last_port] [IP_address]

# scan the vurnerable machine
nmap -p 1-100 192.168.1.70
```

It will give as result:
```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-18 11:28 CET
Nmap scan report for 192.168.1.70
Host is up (0.0062s latency).
Not shown: 94 closed ports
PORT   STATE SERVICE
21/tcp open  ftp
22/tcp open  ssh
23/tcp open  telnet
25/tcp open  smtp
53/tcp open  domain
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.15 seconds
```

In [Goals of Scanning](#23-goals-of-scanning) I told you every machine has 65535 ports. Every `nmap` scanner method only scan the 1000 most common ports but with the `-p` flag you can now scan literally every port of the target.
```bash
nmap -p 1-65535 [IP_address]

# scan the vurnerable machine
nmap -p 1-65535 192.168.1.70
```

This type of scan could, of course, take a lot of time. Since I booted a mini PC with Kali Linux (I'm not no more on a virtual machine), it took just 3 seconds!

It will give as result:
```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-18 11:38 CET
Nmap scan report for 192.168.1.70
Host is up (0.00053s latency).
Not shown: 65505 closed ports
PORT      STATE SERVICE
21/tcp    open  ftp
22/tcp    open  ssh
23/tcp    open  telnet
25/tcp    open  smtp
53/tcp    open  domain
80/tcp    open  http
111/tcp   open  rpcbind
139/tcp   open  netbios-ssn
445/tcp   open  microsoft-ds
512/tcp   open  exec
513/tcp   open  login
514/tcp   open  shell
1099/tcp  open  rmiregistry
1524/tcp  open  ingreslock
2049/tcp  open  nfs
2121/tcp  open  ccproxy-ftp
3306/tcp  open  mysql
3632/tcp  open  distccd
5432/tcp  open  postgresql
5900/tcp  open  vnc
6000/tcp  open  X11
6667/tcp  open  irc
6697/tcp  open  ircs-u
8009/tcp  open  ajp13
8180/tcp  open  unknown
8787/tcp  open  msgsrvr
33488/tcp open  unknown
45842/tcp open  unknown
50335/tcp open  unknown
53138/tcp open  unknown

Nmap done: 1 IP address (1 host up) scanned in 3.20 seconds
```

The main difference, while scanning Metasploitable, is that by scanning all the ports, you can discover the ports `33488/tcp`, `45842/tcp`, `50335/tcp`, `53138/tcp` and some others.

#### 2.4.2.10. Output Nmap Result
```bash
nmap -oN [file-name] [type_of_scan] [IP_address]

# scan the vurnerable machine
nmap -oN output.txt -sT 192.168.1.70
```

It will save the output of the given type of scan in the file you choose (I suggest you to use the `.txt` or `.log` estensions). You will both get the output in the termnail and saved in the file.

You can also output the result in XML format:
```bash
nmap -oX [file-name] [type_of_scan] [IP_address]

# scan the vurnerable machine
nmap -oX output.xml -sT 192.168.1.70
```

#### 2.4.2.11. Bypass Firewall with Nmap
You can not know the exact rules of a firewall. If a port is marked as `filtered`, it's protected by a firewall.
With the `-f` flag enable `nmap` to use the tiny fragmented IP packets. It will split TCP header in different packets to make the detection harder. Note that this type of fragmentation won't always work: it only works if the target network can afford the hip that this scan will cause. 
```bash
sudo nmap -f [IP_address]

# scan the vurnerable machine
sudo nmap -f 192.168.1.70
```

It will give as result:
```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-27 12:47 CET
Nmap scan report for 192.168.1.70
Host is up (0.0041s latency).
Not shown: 978 closed ports
PORT     STATE SERVICE
21/tcp   open  ftp
22/tcp   open  ssh
23/tcp   open  telnet
25/tcp   open  smtp
53/tcp   open  domain
80/tcp   open  http
111/tcp  open  rpcbind
139/tcp  open  netbios-ssn
445/tcp  open  microsoft-ds
512/tcp  open  exec
513/tcp  open  login
514/tcp  open  shell
1099/tcp open  rmiregistry
1524/tcp open  ingreslock
2049/tcp open  nfs
2121/tcp open  ccproxy-ftp
3306/tcp open  mysql
5432/tcp open  postgresql
5900/tcp open  vnc
6000/tcp open  X11
6667/tcp open  irc
8180/tcp open  unknown
MAC Address: 08:00:27:0F:3C:B5 (Oracle VirtualBox virtual NIC)

Nmap done: 1 IP address (1 host up) scanned in 0.52 seconds

```

```bash
sudo nmap -D RND:[number_of_decoys] [type_of_scan] [IP_address]

# scan the vurnerable machine
sudo nmap -D RND:6 -sS 192.168.1.70
```

This option is more focus on hiding your IP address than bypassing security. It creates *decoys* so that the Intrusion Detection System will report them and you, without having the capabilities to determinates which is the real attacker.

In this way, the IP addresses of the decoys are random: your IP will surely be discovered if you are scanning a machine on your local network. Use the command above only in case you're scanning an IP outside your network.
Since you are scanning an IP in the local network, your IP is the only one who is scanning from the same local network.
To make the decoys having real local IP addresses, you just need to specify them separated with a comma (including your):
```bash
sudo nmap -D [1st_decoy_IP],[2nd_decoy_IP][3rd_decoy_IP],[your_IP] [type_of_scan] [IP_address]

# scan the vurnerable machine
sudo nmap -D 192.168.1.10,192.168.1.55,192.168.1.25,ME -sS 192.168.1.70
```

**Please Note** To specify the Kali linux IP address, type `ME`.

#### 2.4.2.12. Security Evasion with Nmap
```bash
sudo nmap -S [IP_address_you_impersonate] -Pn -e [network_interface] -g [allowed_port] [IP_address]

# scan the vurnerable machine
sudo nmap -S 8.8.8.8 -Pn -e eth0 -g 5555 192.168.1.70
```
This option is used to spoof your IP address: it will make the target think that someone else is scanning it. You won't get result of the scan since it will be sent to the IP address you impersonate.
Let's briefly explain all the flags used above:
- `-Pn`: asume that all hosts are online: it doesn't perform the `ping` scan.
- `-e`: specify a network interface. (type `ifconfig` to find out your **network interface**).
- `-g`: specify the source option. It could help to bypass a firewall.

The **fin scan** send a fin packets without any other flags.
```bash
sudo nmap -sF [IP_address]

# scan the vurnerable machine
sudo nmap -sF 192.168.1.70
```

Let's introduce the **timing template** by studing the `-T` flag (on the bottom of `nmap` manual).
`-T` has six mods (options) and first couple of mods are used for IDS evasion: `paranoid` (0) and `sneaky` (1). Since they try to avoid IDS alerts, they take a lot longer to finish. If you are scanning more networks or more machines, this flag could be a waste of time.

```bash
sudo nmap -T [mod] [IP_address]

# scan the vurnerable machine
sudo nmap -T 0 192.168.1.70
```

### 2.4.3. Code a Portscanner in Python3
I usually use **PyCharm** as Python IDE but, since I don't want to weigh down my Kali machine, I'll use [Atom](https://atom.io/).

```bash
# install the .deb packages
cd Downloads
sudo dpkg -i atom-amd64.
```

I won't report the code here since it's saved in *coding* repository but I will comment the code writing some snippets from the code.

`socket` library is needed to communicate with other machines using TCP and UDP protocols.
```python
import socket
```

The first thing to do when creating some type of TCP or UDP connections is initiate a *socket object* (AKA *socket descriptors*).

```python
my_socket = socket.socket()
```

You can now connect the socket object `my_socket` to the target. Note that `ip_address` and `port` will be specified by the user: they are paramters of `scan_port()` function. 

```python
my_socket.connect((ip_address, port))
```

Use a `try/except` block to figure out if the socket succesfully connected to the target.
```python
try:
    my_socket = socket.socket()
    my_socket.connect((ip_address, port))
    print("[+] {} port opened".format( str(port) ))
    my_socket.close()
except:
    print("[-] {} port closed".format( str(port) ))
```

I'll comment the rest of the code in the file itself. For now, let's run the code by scanning the router!
```bash
cd Documents/kali-notes/coding
python3 port-scanner.py
```

Be sure to specify `192.168.1.70` (Metasploitable's IP) and then `100` for the ports to scan so that you got an output like:
```bash
[*] Enter targets to scan (splitted by ,): 192.168.1.70
[*] Enter how many ports you want to scan: 100
```

It will give as result:
```bash
[+] 21 port opened
[+] 22 port opened
[+] 23 port opened
[+] 25 port opened
[+] 53 port opened
[+] 80 port opened

```
